import "dart:async";
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
//

class Utilities {

  static const bool DEBUG = true;


  static appbarHeight(BuildContext context,AppBar appBar){
    double appbarHeight = appBar.preferredSize.height;
    return appbarHeight;
  }


  static double screenWidth(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return screenWidth;
  }

  static double screenHeight(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return screenHeight;
  }

  static showSnackBar(BuildContext context) {
    Scaffold.of(context).showSnackBar(
      SnackBar(content: Text("No Internet Connection")),
    );
  }


  static log(String message) {
    if (DEBUG) print(message);
  }


  static Future<Null> openActivity(context, object) async {
    return await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => object),
    );
  }


  static void replaceActivity(context, object) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => object),
    );
  }

  static replaceNamedActivity(context, routeName) {
    Navigator.of(context).pushReplacementNamed(routeName);
  }

  static void removeStackActivity(context, object) {
    Navigator.pushAndRemoveUntil(
        context, MaterialPageRoute(builder: (context) => object), (r) => false);
  }


  static void closeActivity(context) {
    Navigator.pop(context);
  }


}



