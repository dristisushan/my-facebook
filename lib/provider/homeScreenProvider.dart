import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:myfacebook/base/model.dart';

class HomeScreenProvider with ChangeNotifier{
List <Post> _postList= [];
QuerySnapshot _postSnapshot;
DocumentSnapshot _lastDocument;

get lastDocument => _lastDocument;
set lastDocument(DocumentSnapshot data){
  _lastDocument = data;
  notifyListeners();
}


get postSnapshot => _postSnapshot;
set postSnapshot(QuerySnapshot data){
  _postSnapshot = data;
  notifyListeners();
}

get postList => _postList;
set postList(List<Post> data){
  _postList = data;
  notifyListeners();
}

addPostList(List<Post> data){
  _postList.addAll(data);
  notifyListeners();
}

}