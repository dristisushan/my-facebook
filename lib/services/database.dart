import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:myfacebook/base/model.dart';
import 'package:myfacebook/provider/homeScreenProvider.dart';
import 'package:provider/provider.dart';

class DatabaseService {
  List<DocumentSnapshot> postDocumentSnapshot = List<DocumentSnapshot>();
  final CollectionReference postCollection =
      FirebaseFirestore.instance.collection("posts");


//  final CollectionReference postCollection = Firestore.instance.collection("posts");
  Future addPostData(Post post) async {
    return postCollection.add({
      "profileName": post.profileName,
      "profileImage": post.profileImage,
      "postText": post.postText,
      "images": post.images,
    });
  }

  Future updatePostData(Post post) {
    print("this is post id" + post.id);
    return postCollection.doc(post.id).update({
      "profileName": post.profileName,
      "profileImage": post.profileImage,
      "postText": post.postText,
      "images": post.images,
    });
  }

  List<Post> postList(QuerySnapshot snapshot) {
    return snapshot.docs
        .map((e) => Post(
              postText: e.data()["postText"],
              images: e.data()["images"],
              profileName: e.data()["profileName"],
              profileImage: e.data()["profileImage"],
              id: e.id,
            ))
        .toList();
  }

  Stream<List<Post>> getPostData() {
    return postCollection.snapshots().map((event) => postList(event));
  }

  Future<List<Post>> getPost(BuildContext context) async {
    final data = Provider.of<HomeScreenProvider>(context,listen: false);
    data.postSnapshot =
        await postCollection.limit(10).get();
    if(data.postSnapshot.docs.length  != 0){
      data.lastDocument = data.postSnapshot?.docs[data.postSnapshot.docs.length - 1];
    }
    List<DocumentSnapshot> postDocumentSnapshot = data.postSnapshot.docs;
    List<Post> postList = postDocumentSnapshot
        .map((e) => Post(
              postText: e.data()["postText"],
              images: e.data()["images"],
              profileName: e.data()["profileName"],
              profileImage: e.data()["profileImage"],
              id: e.id,
            ))
        .toList();
    data.postList = postList;
    return postList;
  }



  bool gettingMoreProducts = false;
  bool moreProductAvailable = true;
  Future<List<Post>> getMorePost(BuildContext context) async {
    print("getting more posts called");
    final data = Provider.of<HomeScreenProvider>(context,listen: false);

    if(moreProductAvailable == false){
      return null;
    }

    if(gettingMoreProducts == true){
      print("no more data");
      return null;
    }

    gettingMoreProducts = true;

    data.postSnapshot =
    await postCollection.startAfterDocument(data.lastDocument).limit(10).get();
    if(data.postSnapshot.docs.length < 10){
      moreProductAvailable = false;
    }
    if(data.postSnapshot.docs.length > 0) {
      data.lastDocument =
      data.postSnapshot?.docs[data.postSnapshot.docs.length - 1];


      List<DocumentSnapshot> postDocumentSnapshot = data.postSnapshot.docs;
      List<Post> postList = postDocumentSnapshot
          .map((e) =>
          Post(
            postText: e.data()["postText"],
            images: e.data()["images"],
            profileName: e.data()["profileName"],
            profileImage: e.data()["profileImage"],
            id: e.id,
          ))
          .toList();
      data.addPostList(postList);
      print("post list length" + data.postList.length.toString());
    }
    gettingMoreProducts = false;
    return data.postList;
  }

}
