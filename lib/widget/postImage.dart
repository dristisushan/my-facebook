

import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class PostImage {
  static Widget twoImages(List<Asset> images) {
    return Container(
      child: Row(
        children: [
          Expanded(
              child: Padding(
                  padding: const EdgeInsets.fromLTRB(16, 0, 4, 8),
                  child: Container(
                    child: AssetThumb(
                      asset: images[0],
                      width: 200,
                      height: 200,
                    ),
                  ))),
          images.length == 1
              ? Expanded(child: Container())
              : Expanded(
            child: Padding(
                padding: const EdgeInsets.fromLTRB(4, 0, 16, 8),
                child: Container(
                  child: AssetThumb(
                    asset: images[1],
                    width: 200,
                    height: 200,
                    quality: 100,
                  ),
                )),
          ),
        ],
      ),
    );
  }

  static Widget threeImages(List<Asset> images) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 16, 8),
            child: Container(
              child: AssetThumb(
                asset: images[0],
                width: 200,
                height: 200,
                quality: 100,
              ),
            ),
          ),
          Row(
            children: [
              Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(16, 0, 4, 8),
                    child: AssetThumb(
                      asset: images[1],
                      width: 200,
                      height: 200,
                      quality: 100,
                    ),
                  )),
              Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(4, 0, 16, 8),
                    child: AssetThumb(
                      asset: images[2],
                      width: 200,
                      height: 200,
                      quality: 100,
                    ),
                  )),
            ],
          ),
        ],
      ),
    );
  }

  static Widget fourImages(List<Asset> images) {
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(16, 0, 4, 8),
                  child: AssetThumb(
                    asset: images[0],
                    width: 200,
                    height: 200,
                    quality: 100,
                  ),
                ),
              ),
              Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(4, 0, 16, 8),
                    child: AssetThumb(
                      asset: images[1],
                      width: 200,
                      height: 200,
                      quality: 100,
                    ),
                  )),
            ],
          ),
          Row(
            children: [
              Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(16, 0, 4, 8),
                    child: AssetThumb(
                      asset: images[2],
                      width: 200,
                      height: 200,
                      quality: 100,
                    ),
                  )),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(4, 0, 16, 8),
                  child: AssetThumb(
                    asset: images[3],
                    width: 200,
                    height: 200,
                    quality: 100,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  static Widget moreImage(List<Asset> images) {
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(16, 0, 4, 8),
                    child: AssetThumb(
                      asset: images[0],
                      width: 200,
                      height: 200,
                      quality: 100,
                    ),
                  )),
              Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(4, 0, 16, 8),
                    child: AssetThumb(
                      asset: images[1],
                      width: 200,
                      height: 200,
                      quality: 100,
                    ),
                  )),
            ],
          ),
          Row(
            children: [
              Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(16, 0, 4, 8),
                    child: AssetThumb(
                      asset: images[2],
                      width: 200,
                      height: 200,
                      quality: 100,
                    ),
                  )),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(4, 0, 16, 8),
                  child: Stack(
                    children: [
                      AssetThumb(
                        asset: images[3],
                        width: 200,
                        height: 200,
                        quality: 100,
                      ),
                      Positioned.fill(
                        child: Container(
                          color: Colors.black26,
                        ),
                      ),
                      Positioned.fill(
                        child: Center(
                          child: Text(
                            "${images.length - 3} More",
                            style: TextStyle(fontSize: 18, color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }


}