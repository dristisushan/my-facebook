import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:myfacebook/base/model.dart';
import 'package:myfacebook/base/utilities.dart';
import 'package:myfacebook/screens/editPostScreen.dart';


class PostData extends StatefulWidget {
  final Post post;

  const PostData({
    Key key,
    this.post,
  }) : super(key: key);

  @override
  _PostDataState createState() => _PostDataState();
}

class _PostDataState extends State<PostData> {
  bool isPressedSeeMore = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
          border: Border.all(
            color: Colors.grey,
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(16, 16, 0, 0),
              child: Container(
                child: Row(
                  children: [
                    Container(
                      width: 50.0,
                      height: 50.0,
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                          fit: BoxFit.fill,
                          image: new NetworkImage(widget.post.profileImage),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              widget.post.profileName,
                              style:
                                  TextStyle(fontSize: 18, color: Colors.black),
                            ),
                            PopupMenuButton(
                              elevation: 3.2,
                              initialValue: "",
                              onSelected: (value) {
                                Utilities.openActivity(
                                    context, EditPostScreen(title: "Edit",post: widget.post,));
                              },
                              itemBuilder: (BuildContext context) {
                                return ["Edit"].map((choice) {
                                  return PopupMenuItem(
                                    value: choice,
                                    child: Text(choice),
                                  );
                                }).toList();
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Divider(
              thickness: 1,
              color: Colors.grey,
            ),
            widget.post.postText?.length == 0 || widget.post.postText == null?Container():Padding(
              padding: const EdgeInsets.fromLTRB(16, 8, 16, 16),
              child: Align(
                alignment: Alignment.centerLeft,
                child: RichText(
                  textAlign: TextAlign.justify,
                  text: TextSpan(
                      text: widget.post.postText.length > 100
                          ? isPressedSeeMore == false
                              ? widget.post.postText.substring(0, 100) + "..."
                              : widget.post.postText
                          : widget.post.postText,
                      style: TextStyle(color: Colors.black, fontSize: 18),
                      children: <TextSpan>[
                        TextSpan(
                          text: widget.post.postText.length < 100
                              ? ""
                              : isPressedSeeMore == false
                                  ? ' See more'
                                  : ' See less',
                          style:
                              TextStyle(color: Colors.blueAccent, fontSize: 18),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              isPressedSeeMore = !isPressedSeeMore;
                              setState(() {});
                            },
                        )
                      ]),
                ),
              ),
            ),
            widget.post.images == null
                ? Container()
                : widget.post.images.length == 1
                    ? oneImage()
                    : widget.post.images.length == 2
                        ? twoImages()
                        : widget.post.images.length == 3
                            ? threeImages()
                            : widget.post.images.length == 4
                                ? fourImages()
                                : widget.post.images.length > 4
                                    ? moreImages()
                                    : Container(),
          ],
        ),
      ),
    );
  }

  Widget oneImage(){
    return ConstrainedBox(
      constraints: BoxConstraints(
          maxHeight: Utilities.screenHeight(context) / 2,
          maxWidth: Utilities.screenWidth(context),
          minHeight: 200,
          minWidth: 200
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          child: Image.network(widget.post.images[0]),
        ),
      ),
    );
  }

  Widget twoImages(){
    return Container(
      child: Row(
        children: [
          Expanded(
              child: Padding(
                padding:
                const EdgeInsets.fromLTRB(16, 0, 4, 8),
                child: Image.network(
                  widget.post.images[0],
                ),
              )),
          Expanded(
            child: Padding(
              padding:
              const EdgeInsets.fromLTRB(4, 0, 16, 8),
              child: Image.network(
                widget.post.images[1],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget threeImages(){
    return Container(
      child: Column(
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(
                maxHeight: Utilities.screenHeight(context) * 0.5,
                maxWidth: Utilities.screenWidth(context),
                minHeight: 100,
                minWidth: 100
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(
                  16, 0, 16, 8),
              child: Container(
                  child: Image.network(
                      widget.post.images[0])),
            ),
          ),
          ConstrainedBox(
          constraints: BoxConstraints(
          maxHeight: Utilities.screenHeight(context) * 0.4,
          maxWidth: Utilities.screenWidth(context),
          minHeight: 100,
          minWidth: 100
          ),
            child: Row(
              children: [
                Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(
                          16, 0, 4, 8),
                      child: Image.network(
                        widget.post.images[1],
                      ),
                    )),
                Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(
                          4, 0, 16, 8),
                      child: Image.network(
                        widget.post.images[2],
                      ),
                    )),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget fourImages(){
    return Container(
      child: Column(
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(
                maxHeight: Utilities.screenHeight(context) * 0.4,
                maxWidth: Utilities.screenWidth(context),
                minHeight: 100,
                minWidth: 100
            ),
            child: Row(
              children: [
                Expanded(
                  child: Padding(
                    padding:
                    const EdgeInsets.fromLTRB(
                        16, 0, 4, 8),
                    child: Image.network(
                      widget.post.images[0],
                    ),
                  ),
                ),
                Expanded(
                    child: Padding(
                      padding:
                      const EdgeInsets.fromLTRB(
                          4, 0, 16, 8),
                      child: Image.network(
                        widget.post.images[1],
                      ),
                    )),
              ],
            ),
          ),
          ConstrainedBox(
            constraints: BoxConstraints(
                maxHeight: Utilities.screenHeight(context) * 0.4,
                maxWidth: Utilities.screenWidth(context),
                minHeight: 100,
                minWidth: 100
            ),
            child: Row(
              children: [
                Expanded(
                    child: Padding(
                      padding:
                      const EdgeInsets.fromLTRB(
                          16, 0, 4, 8),
                      child: Image.network(
                        widget.post.images[2],
                      ),
                    )),
                Expanded(
                  child: Padding(
                    padding:
                    const EdgeInsets.fromLTRB(
                        4, 0, 16, 8),
                    child: Image.network(
                      widget.post.images[3],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget moreImages(){
    return Container(
      child: Column(
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(
                maxHeight: Utilities.screenHeight(context) * 0.4,
                maxWidth: Utilities.screenWidth(context),
                minHeight: 100,
                minWidth: 100
            ),
            child: Row(
              children: [
                Expanded(
                    child: Padding(
                      padding:
                      const EdgeInsets.fromLTRB(
                          16, 0, 4, 8),
                      child: Image.network(
                        widget.post.images[0],
                      ),
                    )),
                Expanded(
                    child: Padding(
                      padding:
                      const EdgeInsets.fromLTRB(
                          4, 0, 16, 8),
                      child: Image.network(
                        widget.post.images[1],
                      ),
                    )),
              ],
            ),
          ),
          ConstrainedBox(
            constraints: BoxConstraints(
                maxHeight: Utilities.screenHeight(context) * 0.4,
                maxWidth: Utilities.screenWidth(context),
                minHeight: 100,
                minWidth: 100
            ),
            child: Row(
              children: [
                Expanded(
                    child: Padding(
                      padding:
                      const EdgeInsets.fromLTRB(
                          16, 0, 4, 8),
                      child: Image.network(
                        widget.post.images[2],
                      ),
                    )),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets
                        .fromLTRB(4, 0, 16, 8),
                    child: Stack(
                      children: [
                        Image.network(
                          widget.post.images[3],
                        ),
                        Positioned.fill(
                          child: Container(
                            color:
                            Colors.black26,
                          ),
                        ),
                        Positioned.fill(
                          child: Center(
                            child: Text(
                              "${widget.post.images.length - 3} More",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors
                                      .white),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
