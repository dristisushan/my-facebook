import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myfacebook/base/string.dart';
import 'package:myfacebook/base/utilities.dart';
import 'package:myfacebook/provider/homeScreenProvider.dart';
import 'package:myfacebook/screens/addPostScreen.dart';
import 'package:myfacebook/services/database.dart';
import 'package:myfacebook/widget/postData.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int perScreen = 10;
  bool loadingPost = false;
  bool _initialized = false;
  bool _firebaseError = false;
  List<DocumentSnapshot> posts = [];
  TextEditingController myPost = TextEditingController();
  DocumentSnapshot lastDocument;
  ScrollController _scrollController = ScrollController();

  //   Define an async function to initialize FlutterFire
  initializeFlutterFire() async {
    try {
      // Wait for Firebase to initialize and set `_initialized` state to true
      await Firebase.initializeApp();
      setState(() {
        _initialized = true;
      });
      return "ok";
    } catch (e) {
      // Set `_error` state to true if Firebase initialization fails
      setState(() {
        _firebaseError = true;
      });
      return "fail";
    }
  }



  @override
  void initState() {
    setState(() {
      loadingPost = true;
    });
    // TODO: implement initState
    initializeFlutterFire().then((data) {
      DatabaseService().getPost(context).then((data) {
        setState(() {
          loadingPost = false;
        });
      });
    });
    _scrollController.addListener(() {
      double maxScroll = _scrollController.position.maxScrollExtent;
      double currentScroll = _scrollController.position.pixels;
      if (maxScroll == currentScroll) {
        DatabaseService().getMorePost(context);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: Text("My Facebook"),
          centerTitle: true,
        ),
        body: loadingPost
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Consumer<HomeScreenProvider>(
                builder: (context, posts, _) {
                  return ListView.builder(
                      controller: _scrollController,
                      itemCount:
                          posts.postList?.length == 0 || posts.postList == null
                              ? 2
                              : posts.postList.length + 1,
                      itemBuilder: (context, i) {
                        return i == 0
                            ? Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(10, 10, 10, 0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    border: Border.all(
                                        color: Colors.grey, width: 1),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        16, 16, 16, 16),
                                    child: Row(
                                      children: [
                                        new Container(
                                          width: 50.0,
                                          height: 50.0,
                                          decoration: new BoxDecoration(
                                            shape: BoxShape.circle,
                                            image: new DecorationImage(
                                              fit: BoxFit.fill,
                                              image: new NetworkImage(
                                                  Strings.userImage),
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 20,
                                        ),
                                        Expanded(
                                            child: InkWell(
                                          onTap: () {
                                            Utilities.openActivity(
                                                context,
                                                AddPostScreen(
                                                  title: "Add Post",
                                                ));
                                          },
                                          child: Container(
                                            height: 50,
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(10),
                                                ),
                                                border: Border.all(
                                                  color: Colors.grey,
                                                )),
                                            child: Align(
                                                alignment: Alignment.centerLeft,
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 8.0),
                                                  child: Text(
                                                    "What's on your mind?",
                                                    style:
                                                        TextStyle(fontSize: 18),
                                                  ),
                                                )),
                                          ),
                                        )),
                                      ],
                                    ),
                                  ),
                                ),
                              )
                            : posts.postList?.length == 0 ||
                                    posts.postList == null
                                ? Container(
                                    height:
                                        Utilities.screenHeight(context) - 190,
                                    child: Center(child: Text("No data")))
                                : PostData(post: posts.postList[i - 1]);
                      });
                },
              ),
      ),
    );
  }
}
