//import 'package:cloud_firestore/cloud_firestore.dart';
//import 'package:firebase_core/firebase_core.dart';
//import 'package:flutter/cupertino.dart';
//import 'package:flutter/material.dart';
//import 'package:provider/provider.dart';
//
//class HomeScreen extends StatefulWidget {
//  @override
//  _HomeScreenState createState() => _HomeScreenState();
//}
//
//class _HomeScreenState extends State<HomeScreen> {
//  int index = 5;
//  bool _initialized = false;
//  bool _firebaseError = false;
//  TextEditingController myPost = TextEditingController();
//
//  //   Define an async function to initialize FlutterFire
//  void initializeFlutterFire() async {
//    try {
//      // Wait for Firebase to initialize and set `_initialized` state to true
//      await Firebase.initializeApp();
//      setState(() {
//        _initialized = true;
//      });
//    } catch (e) {
//      // Set `_error` state to true if Firebase initialization fails
//      setState(() {
//        _firebaseError = true;
//      });
//    }
//  }
//
//  @override
//  void initState() {
//    // TODO: implement initState
//    initializeFlutterFire();
//    super.initState();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return !_initialized
//        ? Container(
//        color: Colors.white,
//        child: Center(child: CircularProgressIndicator()))
//        : StreamProvider<List<Post>>.value(
//      value: DatabaseService().getPostData(),
//      child: SafeArea(
//        child: Scaffold(
//          appBar: AppBar(
//            backgroundColor: Colors.blue,
//            title: Text("My Facebook"),
//            centerTitle: true,
//          ),
//          body: Consumer<List<Post>>(
//            builder: (context, posts, _) {
//              return ListView.builder(
//                  itemCount: posts?.length == 0 || posts == null
//                      ? 2
//                      : posts.length + 1,
//                  itemBuilder: (context, i) {
//                    return i == 0
//                        ? Padding(
//                      padding:
//                      const EdgeInsets.fromLTRB(10, 10, 10, 0),
//                      child: Container(
//                        decoration: BoxDecoration(
//                          borderRadius:
//                          BorderRadius.all(Radius.circular(10)),
//                          border: Border.all(
//                              color: Colors.grey, width: 1),
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.fromLTRB(
//                              16, 16, 16, 16),
//                          child: Row(
//                            children: [
//                              new Container(
//                                width: 50.0,
//                                height: 50.0,
//                                decoration: new BoxDecoration(
//                                  shape: BoxShape.circle,
//                                  image: new DecorationImage(
//                                    fit: BoxFit.fill,
//                                    image: new NetworkImage(
//                                        "https://i.imgur.com/BoN9kdC.png"),
//                                  ),
//                                ),
//                              ),
//                              SizedBox(
//                                width: 20,
//                              ),
//                              Expanded(
//                                  child: InkWell(
//                                    onTap: () {
//                                      Utilities.openActivity(
//                                          context,
//                                          AddPostScreen(
//                                            title: "Add Post",
//                                          ));
//                                    },
//                                    child: Container(
//                                      height: 50,
//                                      decoration: BoxDecoration(
//                                          borderRadius:
//                                          BorderRadius.all(
//                                            Radius.circular(10),
//                                          ),
//                                          border: Border.all(
//                                            color: Colors.grey,
//                                          )),
//                                      child: Align(
//                                          alignment:
//                                          Alignment.centerLeft,
//                                          child: Padding(
//                                            padding:
//                                            const EdgeInsets.only(
//                                                left: 8.0),
//                                            child: Text(
//                                              "What's on your mind?",
//                                              style: TextStyle(
//                                                  fontSize: 18),
//                                            ),
//                                          )),
//                                    ),
//                                  )),
//                            ],
//                          ),
//                        ),
//                      ),
//                    )
//                        : posts?.length == 0 || posts == null
//                        ? Container(
//                        height:
//                        Utilities.screenHeight(context) - 190,
//                        child: Center(child: Text("No data")))
//                        : PostData(post: posts[i - 1]);
//                  });
//            },
//          ),
//        ),
//      ),
//    );
//  }
//}
