import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:myfacebook/base/model.dart';
import 'package:myfacebook/base/string.dart';
import 'package:myfacebook/base/utilities.dart';
import 'package:myfacebook/screens/homeScreen.dart';
import 'package:myfacebook/services/database.dart';
import 'package:myfacebook/widget/postImage.dart';
import 'package:permission_handler/permission_handler.dart';

class AddPostScreen extends StatefulWidget {
  final String title;

  const AddPostScreen({Key key, this.title = ""}) : super(key: key);

  @override
  _AddPostScreenState createState() => _AddPostScreenState();
}

class _AddPostScreenState extends State<AddPostScreen> {
  List<Asset> images = List<Asset>();
  String _error = 'No Error Dectected';
  TextEditingController postController;
  bool noPost = true;
  bool noImage = true;
  bool isLoading = false;
  final _key = GlobalKey<ScaffoldState>();
  Future<bool> checkAndRequestCameraPermissions() async {
    PermissionStatus permission = await Permission.camera.status;
    if (permission.isUndetermined) {
      Map<Permission, PermissionStatus> status = await [
        Permission.storage,
        Permission.camera,
      ].request();
    }
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Example App",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
      if (images.length == 0) {
        noImage = true;
        setState(() {});
      } else {
        noImage = false;
        setState(() {});
      }
    } on Exception catch (e) {
      error = e.toString();
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
    setState(() {
      images = resultList;
      _error = error;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    postController = TextEditingController();
    checkAndRequestCameraPermissions();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key,
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          Container(
            child: IconButton(
              disabledColor: Colors.grey.withOpacity(0.5),
              onPressed: () {
                if(noPost && images.length == 0){
                  return null;
                }else{
                  isLoading = true;
                  setState(() {});
                  images.length == 0
                      ? DatabaseService()
                      .addPostData(Post(
                      profileName: Strings.userName,
                      profileImage: Strings.userImage,
                      postText: postController.text,
                      images: null))
                      .then((_) {
                    SnackBar snackbar =
                    SnackBar(content: Text('Uploaded Successfully'));
                    _key.currentState.showSnackBar(snackbar);
                    isLoading = false;
                    setState(() {});
                    Timer(Duration(seconds: 2), (){
                      Utilities.removeStackActivity(context,HomeScreen());
                    });
                  })
                      : uploadImages();
                }

              },
              icon: Icon(
                Icons.done,
                color:
                    noPost && images.length == 0 ? Colors.grey : Colors.white,
              ),
            ),
          )
        ],
      ),
      body:
//      !_initialized?CircularProgressIndicator():
          Stack(
        children: [
          Container(
              child: Column(
            children: [
              Expanded(
                child: ListView(
                  children: [
                    TextFormField(
                      controller: postController,
                      decoration: new InputDecoration(
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          contentPadding: EdgeInsets.only(
                              left: 15, bottom: 11, top: 11, right: 15),
                          hintText: "What's on your mind?"),
                      onChanged: (data) {
                        if (data.length == 0) {
                          noPost = true;
                          setState(() {});
                        } else {
                          noPost = false;
                          setState(() {});
                        }
                      },
                      maxLines: null,
                      keyboardType: TextInputType.multiline,
                    ),
//                  buildGridView(),
                    images.length == 1
                        ? PostImage.twoImages(images)
                        : images.length == 2
                            ? PostImage.twoImages(images)
                            : images.length == 3
                                ? PostImage.threeImages(images)
                                : images.length == 4
                                    ? PostImage.fourImages(images)
                                    : images.length > 4
                                        ? PostImage.moreImage(images)
                                        : Container(),
                  ],
                ),
              ),
              Divider(
                color: Colors.grey,
                thickness: 1,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: 40,
                  child: Row(
                    children: [
                      Icon(
                        Icons.image,
                        color: Colors.blue,
                      ),
                      InkWell(
                          onTap: () {
                            loadAssets();
                          },
                          child: Container(child: Text("Add Images"))),
                    ],
                  ),
                ),
              )
            ],
          )),
          isLoading == false
              ? Container()
              : Align(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(),
                ),
        ],
      ),
    );
  }

  List<String> imageUrls = <String>[];

  // Set default `_initialized` and `_error` state to false

  void uploadImages() async {
    for (var imageFile in images) {
      postImage(imageFile).then((downloadUrl) {
        imageUrls.add(downloadUrl.toString());
        if (imageUrls.length == images.length) {
          DatabaseService()
              .addPostData(Post(
                  profileName: Strings.userName,
                  profileImage: Strings.userImage,
                  postText: postController.text,
                  images: imageUrls))
              .then((_) {
            SnackBar snackbar =
                SnackBar(content: Text('Uploaded Successfully'));
            _key.currentState.showSnackBar(snackbar);
            isLoading = false;
            setState(() {});
            Timer(Duration(seconds: 2), (){
              Utilities.removeStackActivity(context,HomeScreen());
            });
          });
        }
      }).catchError((err) {
        print(err);
      });
    }
  }


  Future<dynamic> postImage(Asset imageFile) async {
    String fileName = DateTime.now().millisecondsSinceEpoch.toString();
    StorageReference reference = FirebaseStorage.instance.ref().child(fileName);
    StorageUploadTask uploadTask =
        reference.putData((await imageFile.getByteData()).buffer.asUint8List());
    StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
    return storageTaskSnapshot.ref.getDownloadURL();
  }
}
