import 'dart:async';
import 'dart:convert';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:myfacebook/base/model.dart';
import 'package:myfacebook/base/utilities.dart';
import 'package:myfacebook/screens/homeScreen.dart';
import 'package:myfacebook/services/database.dart';
import 'package:myfacebook/widget/postImage.dart';
import 'package:permission_handler/permission_handler.dart';

class EditPostScreen extends StatefulWidget {
  final String title;
  final Post post;

  const EditPostScreen({Key key, this.title, this.post}) : super(key: key);

  @override
  _EditPostScreenState createState() => _EditPostScreenState();
}

class _EditPostScreenState extends State<EditPostScreen> {
  List<Asset> images = List<Asset>();
  String _error = 'No Error Dectected';
  TextEditingController postController;
  bool changeInPost = false;
  bool addNewImage = false;
  bool isLoading = false;
  final _globalKey = GlobalKey<ScaffoldState>();
  List<String> postImages = [];

  Future<bool> checkAndRequestCameraPermissions() async {
    PermissionStatus permission = await Permission.camera.status;
    if (permission.isUndetermined) {
      Map<Permission, PermissionStatus> status = await [
        Permission.storage,
        Permission.camera,
      ].request();
    }
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Example App",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
      if (images.length == 0) {
        addNewImage = false;
        setState(() {});
      } else {
        addNewImage = true;
        setState(() {});
      }
    } on Exception catch (e) {
      error = e.toString();
    }
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
    setState(() {
      images = resultList;
      _error = error;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    postController = TextEditingController(text: widget.post.postText);
    print("this is id" + widget.post.id);
    checkAndRequestCameraPermissions();
    if (widget.post.images == null) {
      postImages = [];
    } else {
      postImages = widget.post.images.map((e) => e.toString()).toList();
    }
//    postImages = widget.post.images;
    print("this is post images" + postImages.toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          Container(
            child: IconButton(
              disabledColor: Colors.grey.withOpacity(0.5),
              onPressed: () {
                if (changeInPost == false && images.length == 0) {
                  return null;
                } else {
                  isLoading = true;
                  setState(() {});
                  images.length == 0
                      ? DatabaseService()
                          .updatePostData(Post(
                              profileName: widget.post.profileName,
                              profileImage: widget.post.profileImage,
                              postText: postController.text,
                              images: postImages,
                              id: widget.post.id))
                          .then((_) {
                          SnackBar snackbar =
                              SnackBar(content: Text('Uploaded Successfully'));
                          _globalKey.currentState.showSnackBar(snackbar);
                          isLoading = false;
                          setState(() {});
                          Timer(Duration(seconds: 2), () {
                            Utilities.removeStackActivity(context,HomeScreen());
                          });
                        })
                      : uploadImages();
                }
              },
              icon: Icon(
                Icons.done,
                color: changeInPost == false && images.length == 0
                    ? Colors.grey
                    : Colors.white,
              ),
            ),
          )
        ],
      ),
      body: Stack(
        children: [
          Container(
              child: Column(
            children: [
              Expanded(
                child: ListView(
                  children: [
                    TextFormField(
                      controller: postController,
                      decoration: new InputDecoration(
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          contentPadding: EdgeInsets.only(
                              left: 15, bottom: 11, top: 11, right: 15),
                          hintText: "What's on your mind?"),
                      onChanged: (data) {
                        if (data == widget.post.postText) {
                          changeInPost = false;
                          setState(() {});
                        } else {
                          changeInPost = true;
                          setState(() {});
                        }
                      },
                      maxLines: null,
                      keyboardType: TextInputType.multiline,
                    ),
                    widget.post.images == null
                        ? Container()
                        : widget.post.images?.length == 1
                            ? twoImages()
                            : widget.post.images.length == 2
                                ? twoImages()
                                : widget.post.images.length == 3
                                    ? threeImages()
                                    : widget.post.images.length == 4
                                        ? fourImages()
                                        : widget.post.images.length > 4
                                            ? moreImage()
                                            : Container(),
                    images.length == 0
                        ? Container()
                        : Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Text(
                              "New Image",
                              style: TextStyle(
                                  color: Colors.red,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                    images.length == 1
                        ? PostImage.twoImages(images)
                        : images.length == 2
                            ? PostImage.twoImages(images)
                            : images.length == 3
                                ? PostImage.threeImages(images)
                                : images.length == 4
                                    ? PostImage.fourImages(images)
                                    : images.length > 4
                                        ? PostImage.moreImage(images)
                                        : Container(),
                  ],
                ),
              ),
              Divider(
                color: Colors.grey,
                thickness: 1,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: 40,
                  child: Row(
                    children: [
                      Icon(
                        Icons.image,
                        color: Colors.blue,
                      ),
                      InkWell(
                          onTap: () {
                            loadAssets();
                          },
                          child: Container(child: Text("Add Images"))),
                    ],
                  ),
                ),
              )
            ],
          )),
          isLoading == false
              ? Container()
              : Align(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(),
                ),
        ],
      ),
    );
  }

  Widget twoImages() {
    return Container(
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxHeight: Utilities.screenHeight(context) * 0.3,
          maxWidth: Utilities.screenWidth(context),
          minHeight: 100,
          minWidth: 100,
        ),
        child: Row(
          children: [
            Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(16, 0, 4, 8),
                    child: Container(
                        child: Image.network(widget.post.images[0])))),
            widget.post.images.length == 1
                ? Expanded(child: Container())
                : Expanded(
                    child: Padding(
                        padding: const EdgeInsets.fromLTRB(4, 0, 16, 8),
                        child: Container(
                            child: Image.network(widget.post.images[1]))),
                  ),
          ],
        ),
      ),
    );
  }

  Widget threeImages() {
    return Container(
      child: Column(
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(
              maxHeight: Utilities.screenHeight(context) * 0.2,
              maxWidth: Utilities.screenWidth(context),
              minHeight: 100,
              minWidth: 100,
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16, 0, 16, 8),
              child: Container(child: Image.network(widget.post.images[0])),
            ),
          ),
          ConstrainedBox(
            constraints: BoxConstraints(
              maxHeight: Utilities.screenHeight(context) * 0.2,
              maxWidth: Utilities.screenWidth(context),
              minHeight: 100,
              minWidth: 100,
            ),
            child: Row(
              children: [
                Expanded(
                    child: Padding(
                        padding: const EdgeInsets.fromLTRB(16, 0, 4, 8),
                        child: Image.network(widget.post.images[1]))),
                Expanded(
                    child: Padding(
                        padding: const EdgeInsets.fromLTRB(4, 0, 16, 8),
                        child: Image.network(widget.post.images[2]))),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget fourImages() {
    return Container(
      child: Column(
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(
              maxHeight: Utilities.screenHeight(context) * 0.2,
              maxWidth: Utilities.screenWidth(context),
              minHeight: 100,
              minWidth: 100,
            ),
            child: Row(
              children: [
                Expanded(
                  child: Padding(
                      padding: const EdgeInsets.fromLTRB(16, 0, 4, 8),
                      child: Image.network(widget.post.images[0])),
                ),
                Expanded(
                    child: Padding(
                        padding: const EdgeInsets.fromLTRB(4, 0, 16, 8),
                        child: Image.network(widget.post.images[1]))),
              ],
            ),
          ),
          ConstrainedBox(
            constraints: BoxConstraints(
              maxHeight: Utilities.screenHeight(context) * 0.2,
              maxWidth: Utilities.screenWidth(context),
              minHeight: 100,
              minWidth: 100,
            ),
            child: Row(
              children: [
                Expanded(
                    child: Padding(
                        padding: const EdgeInsets.fromLTRB(16, 0, 4, 8),
                        child: Image.network(widget.post.images[2]))),
                Expanded(
                  child: Padding(
                      padding: const EdgeInsets.fromLTRB(4, 0, 16, 8),
                      child: Image.network(widget.post.images[3])),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget moreImage() {
    return Container(
      child: Column(
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(
              maxHeight: Utilities.screenHeight(context) * 0.2,
              maxWidth: Utilities.screenWidth(context),
              minHeight: 100,
              minWidth: 100,
            ),
            child: Row(
              children: [
                Expanded(
                    child: Padding(
                        padding: const EdgeInsets.fromLTRB(16, 0, 4, 8),
                        child: Image.network(widget.post.images[0]))),
                Expanded(
                    child: Padding(
                        padding: const EdgeInsets.fromLTRB(4, 0, 16, 8),
                        child: Image.network(widget.post.images[1]))),
              ],
            ),
          ),
          ConstrainedBox(
            constraints: BoxConstraints(
              maxHeight: Utilities.screenHeight(context) * 0.2,
              maxWidth: Utilities.screenWidth(context),
              minHeight: 100,
              minWidth: 100,
            ),
            child: Row(
              children: [
                Expanded(
                    child: Padding(
                        padding: const EdgeInsets.fromLTRB(16, 0, 4, 8),
                        child: Image.network(widget.post.images[2]))),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(4, 0, 16, 8),
                    child: Stack(
                      children: [
                        Image.network(widget.post.images[3]),
                        Positioned.fill(
                          child: Container(
                            color: Colors.black26,
                          ),
                        ),
                        Positioned.fill(
                          child: Center(
                            child: Text(
                              "${widget.post.images.length - 3} More",
                              style:
                                  TextStyle(fontSize: 18, color: Colors.white),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  List imageUrls = <String>[];

  // Set default `_initialized` and `_error` state to false

  void uploadImages() async {
    for (var imageFile in images) {
      postImage(imageFile).then((downloadUrl) {
        imageUrls.add(downloadUrl.toString());
        if (imageUrls.length == images.length) {
          imageUrls.addAll(postImages);
          DatabaseService()
              .updatePostData(Post(
                  profileName: widget.post.profileName,
                  profileImage: widget.post.profileImage,
                  postText: postController.text,
                  images: imageUrls,
                  id: widget.post.id))
              .then((_) {
            SnackBar snackbar = SnackBar(content: Text('Update Successfully'));
            _globalKey.currentState.showSnackBar(snackbar);
            isLoading = false;
            setState(() {});
            Timer(Duration(seconds: 2), () {
              Utilities.removeStackActivity(context,HomeScreen());
            });
          });
        }
      }).catchError((err) {
        print(err);
      });
    }
  }

  Future<dynamic> postImage(Asset imageFile) async {
    String fileName = DateTime.now().millisecondsSinceEpoch.toString();
    StorageReference reference = FirebaseStorage.instance.ref().child(fileName);
    StorageUploadTask uploadTask =
        reference.putData((await imageFile.getByteData()).buffer.asUint8List());
    StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
    return storageTaskSnapshot.ref.getDownloadURL();
  }
}
