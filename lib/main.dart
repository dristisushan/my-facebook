import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:myfacebook/provider/homeScreenProvider.dart';
import 'package:myfacebook/screens/homeScreen.dart';
import 'package:provider/provider.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => HomeScreenProvider(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: HomeScreen(),
      ),
    );
  }
}


//class MyHomePage extends StatefulWidget {
//  @override
//  _MyHomePageState createState() => _MyHomePageState();
//}
//
//class _MyHomePageState extends State<MyHomePage> {
//  final _globalKey = GlobalKey<ScaffoldState>();
//  @override
//  Widget build(BuildContext context) {
//    return DefaultTabController(
//      length: 2,
//      child: Scaffold(
//        key: _globalKey,
//        backgroundColor: Theme.of(context).backgroundColor,
//        appBar: AppBar(
//          title: Text('Multiple Images'),
//          bottom:   TabBar(
//            tabs: [
//              Tab(icon: Icon(Icons.image),text: 'Images',),
//              Tab(icon: Icon(Icons.cloud_upload),text: "Upload Images",),
//            ],
//            indicatorColor: Colors.red,
//            indicatorWeight: 5.0,
//          ),
//        ),
//
//        body: TabBarView(
//          children: <Widget>[
////            ViewImages(),
//            ViewImages(),
//            UploadImages(globalKey: _globalKey,),
//
//          ],
//        ),
//      ),
//    );
//  }
//}
//
//
//class ViewImages extends StatelessWidget {
//  List<NetworkImage> _listOfImages = <NetworkImage>[];
//
//  @override
//  Widget build(BuildContext context) {
//    return Column(
//      children: <Widget>[
//        SizedBox(
//          height: 20,
//        ),
//        Flexible(
//            child: StreamBuilder<QuerySnapshot>(
//                stream: Firestore.instance.collection('images').snapshots(),
//                builder: (context, snapshot) {
//                  if (snapshot.hasData) {
//                    return ListView.builder(
//                        itemCount: snapshot.data.documents.length,
//                        itemBuilder: (BuildContext context, int index) {
//                          _listOfImages = [];
//                          for (int i = 0;
//                          i <
//                              snapshot.data.documents[index].data['urls']
//                                  .length;
//                          i++) {
//                            _listOfImages.add(NetworkImage(snapshot
//                                .data.documents[index].data['urls'][i]));
//                          }
//                          return Column(
//                            children: <Widget>[
//                              Container(
//                                margin: EdgeInsets.all(10.0),
//                                height: 200,
//                                decoration: BoxDecoration(
//                                  color: Colors.white,
//                                ),
//                                width: MediaQuery.of(context).size.width,
//                                child: Carousel(
//                                    boxFit: BoxFit.cover,
//                                    images: _listOfImages,
//                                    autoplay: false,
//                                    indicatorBgPadding: 5.0,
//                                    dotPosition: DotPosition.bottomCenter,
//                                    animationCurve: Curves.fastOutSlowIn,
//                                    animationDuration:
//                                    Duration(milliseconds: 2000)),
//                              ),
//                              Container(
//                                height: 1,
//                                width: MediaQuery.of(context).size.width,
//                                color: Colors.red,
//                              )
//                            ],
//                          );
//                        });
//                  } else {
//                    return Center(
//                      child: CircularProgressIndicator(),
//                    );
//                  }
//                }))
//      ],
//    );
//  }
//}